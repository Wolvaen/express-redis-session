/**
 * Created by andre on 19.05.16.
 */


/*!
 * redis-session
 * Copyright 2016 by André Leifeld
 * MIT Licensed
 */

'use strict';

var util = require('util'),
    Store = require('express-session/session/store'),
    sprintf = require('sprintf-js'),
    SESSION_KEY = 'APP_SESS_%s';

/**
 * A session store in redis.
 */

function RedisSessionStore(redisClient) {
    Store.call(this);
    this.redisClient = redisClient;
}
RedisSessionStore.prototype = Object.create(Store.prototype);
RedisSessionStore.prototype.constructor = RedisSessionStore;

RedisSessionStore.prototype.__rrIndex = 0;

/*
 *  Get redis connection
 */

RedisSessionStore.prototype.getRedis = function(slave) {
    if (this.redisClient.constructor !== 'RedisClient') {
        if (slave && this.redisClient.hasOwnProperty('slaves')) {
            if (this.__rrIndex < this.redisClient.slaves.length-1) {
                this.__rrIndex++;
            } else {
                this.__rrIndex = 0;
            }
            return this.redisClient.slaves[this.__rrIndex];
        } else {
            return this.redisClient.default;
        }
    }
    return this.redisClient;
};

/**
 * Get session from redis.
 */

RedisSessionStore.prototype.getSession = function(id, callback) {
    var sessKey = sprintf(SESSION_KEY, id);
    getRedis('sesion').get(sessKey, function(err, result) {
        var session, expires;
        if (!err && result) {
            session = JSON.parse(sess);
            expires = typeof sess.cookie.expires === 'string'
                ? new Date(sess.cookie.expires)
                : sess.cookie.expires;
            if (expires && expires <= Date.now()) {
                session = null;
                this.getRedis().del(sessKey)
            }
            typeof callback === 'function' && callback(err, session);
        }
    });
};

/**
 * Clear all sessions.
 */

RedisSessionStore.prototype.clear = function clear(callback) {
    this.getRedis().del(sprintf(SESSION_KEY, '*'), function(err, result) {
        typeof callback === 'function' && callback(err, result);
    });
};

/**
 * Destroy the session associated with the given session ID.
 */

RedisSessionStore.prototype.destroy = function destroy(id, callback) {
    this.getRedis().del(sprintf(SESSION_KEY, id), function(err, result) {
        typeof callback === 'function' && callback(err, result);
    });
};

/**
 * Fetch session by the given session ID.
 */

RedisSessionStore.prototype.get = RedisSessionStore.prototype.getSession;

/**
 * Get number of active sessions.
 */

RedisSessionStore.prototype.length = function length(callback) {
    this.getRedis().mget(sprintf(SESSION_KEY, '*'), function (err, result) {
        if (err) { return callback(err, null); }
        callback(null, Object.keys(result).length);
    });
};

/**
 * Commit the given session associated with the given sessionId to the store.
 */

RedisSessionStore.prototype.set = function set(id, session, callback) {
    var jsonSess = JSON.stringify(session);
    this.getRedis().set(sprintf(SESSION_KEY, id), jsonSess, callback);
};

/**
 * Touch the given session object associated with the given session ID.
 */

RedisSessionStore.prototype.touch = function touch(id, session, callback) {
    this.getSession(id, function(err, result) {
        if (!err && result) {
            result.cookie = session.cookie;
            this.getRedis(false).set(sprintf(SESSION_KEY, id), JSON.stringify(result), callback);
        }
        typeof callback === 'function' && callback(err, null);
     });
};

/**
 * Module exports.
 */

module.exports = function(redisClient) {
    if (redisClient.constructor.name !== 'RedisClient' ||
        (redisClient.hasOwnProperty('default') && redisClient.hasOwnProperty('slaves'))) {

        return new Error('Instance or master/slave cluster array of RedisClient needed!');
    }
    return new RedisSessionStore(redisClient);
};